//Se incluye la librería que provee el fabricante del OLED
//las librerías se deben instalar en el IDE Arduino previamente.
#include <Adafruit_SSD1306.h>

//Se define las constantes
#define SCREEN_WIDTH 128 //Define pixeles el ancho de la pantalla.
#define SCREEN_HEIGHT 64 //Define pixeles el alto de la pantalla.

/* Se declara e inicia el objeto display de la librería de la pantalla 
y se pasan las constantes de configuración como parámetros */
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT);

void setup() {
  delay(1000); // Pausa por 1 segundo 
  Serial.begin(9600); //Iniciar comunicación Serial

  /* La función display.begin() inicia la pantalla y envía como parámetro la dirección 0x3C,
  si quiere verificar la dirección de su pantalla puede correr el programa TEST_OLED en el apéndice.
  El ciclo if se encarga de que si inicia con error envía un mensaje por el monitor serial */
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

}
void loop(){
  display.display();//Muestra en pantalla el contenido
  delay(1000); // Pausa por 1 segundo para mantener la pantalla congelada
  display.clearDisplay(); //limpia el la pantalla

  display.setTextSize(2); // Configura el tamaño del texto
  display.setTextColor(SSD1306_WHITE);  //Configura el color del texto
  display.setCursor(0,0);// Ubica el cursor en la pantalla en la posición (0,0)
  display.println(F("EJEMPLO"));
  display.setTextSize(2);// Configura el tamaño del texto
  display.setTextColor(SSD1306_WHITE);//Configura el color del texto
  display.setCursor(0,16);//Ubica el cursor de pantalla en posición(0,16)
  display.println(F("Taller # 1"));
}
